# Contributing

If you contrubute, please keep this in mind:

    - If your contribution is based on research, please include references. Books/pages/links in footnotes would be best.
    - If your contribution is fluff related, please keep it to yourself for now.
    - Please spellcheck your work, and use english.

This is still very early in the development of this system. The requirements and/or system for publishing may change at any time.
