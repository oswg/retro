#### Self-Evident Truths
1. Codes of Conduct exist so non-programmers/non-designers, anti-programmers/anti-hobbiests and middle-managers can better control a project that they do not own.
2. Codes of Conduct only hurt those who do not enforce a code of conduct.
3. Only moral busybodies enforce a Code of Conduct, and thus they can gain control of a product they otherwise do not contribute to in any meaningful way.
4. Codes of Conduct inevitably generate drama and strife between team members, largely ending in either a schism, balkanization, or bully tactics to remove individuals.
5. Codes of Conduct will invariably be controlled on a long enough timeline by zealots - and thus enforce their increasingly radical worldview on team members.

#### Response
Seeing these self-evident truths are in direct opposition to a functional programming/development team, my goals in creating a Code of Conduct are as follows:

1. Must not be enforcable solely by moral busybodies.
2. Must be free as in freedom
3. Must be consistant with freedom of speech as outlined by John Stuart Mill:
 > If the arguments of the present chapter are of any validity, there ought to exist the fullest liberty of professing and discussing, as a matter of ethical conviction, any doctrine, however immoral it may be considered.


Thereby, a Code of Conduct must not exist, for it impinges upon the ability of the programmer to express their thoughts and ideas. All ideas must be able to be expressed; however heterodox they may be.


## Code of Conduct

### Act in a professional, civil manner whilst interacting with teammates or those asking questions about the project.

If I need to define "professional" or "civil", I shall. However, if I am required to do so, perhaps your idea of a Code of Conduct is really a syllabus or some kind of binding contract.


I am of the opinion that if you have gone to the trouble to seek this project out, then you are likely not a mental invalid or particularly immature in emotional or mental strength. 

If you have a grievance with this document, you may contact me. I will give your grievance all the respect it deserves.

Signed,

 *OSWG Team*

   Owner
