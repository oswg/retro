# Requirements

1. Must be a functional wargaming system.
	- Contains systems for:
		- Attack
			- Melee
			- Ranged
			- Magic/Psychic/Faith
			- Poison/Drugs/Debuff
		- Damage
			- Types
		- Defense
			- Armor
			- Invulnerability
			- Magical/Psychic/Faith
			- Debuff
		- Support
			- Buffs
				- Movement increase
				- Armor increase
				- etc
			- Movement (as in to carry)
			- Healing/Raising
		- Death of Units
			- Through instadeath/loss of HP or similar
		- Personal Stats
			- Health
			- Armor
			- Weapon Competancy
			- Movement
			- Resistance to debuffs
			- Invulnerability
			- Initiative
			- Leadership
		- Leadership/Morale
		- Movement
		- Environmental Changes/buffs/debuffs
		- Line-of-sight
		- Vehicular movement
		- Vehicular combat/defense
		- Vehicular Stats
		- Unit Deployment
		- Varying point costs
		- Turn determination
		- Differing factional combat
		- Meta-combat support
2. Must use a points-based or other balancing system
	- This system must allow and promote user creativity, without unbalancing combat.
3. Must be universe agnostic.
	- Can be modified to fit any time period, real or fictional
4. Must be internally consistant.
	- No contradictory rules
5. Must be extensible.
	- Must be able to expand upon the ruleset through the use of an external book or influence
6. Must be fun
	- Has to be fun. Else, why bother.
7. Must be user customizable
	- Users must have the ability to modify rules for specific engagements, and there must be examples of such.
8. Must be dynamic
	- The rules should be malleable based upon the creativity of the users; custom units, modifyable terrain, etc.


