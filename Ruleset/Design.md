# Design (Currently spitball)

## What is a wargame?
A Wargame is a cooperative or competative event involving two or more players, simulating combat using minatures and randomness using strategy.

## Context
What is the contextual universe to the highest meaningful level?

 1. Other Wargames are games. 
 2. Other Wargames deal with combat
 3. Other Wargames use some kind of randomness
 4. Other wargames use dice to resolve randomness
 5. Other wargames use armies per side in order to create conflict
 6. Other wargames' armies are different in some capacity 
 7. Other wargames deal with the 2 dimensional battlefield in different ways

## Design Decisions
What are the questions that must be answered in order to create a new wargame?
1. Feeling
	- Small, tactical vs large & epic
	- Luck vs Tactics
	- Accessibility vs Depth
	- Tactics in Combat vs Tactics in Resource Management
	- Vehicles are just big units vs Vehicles are completely different
2. Mechanics
    - Missions vs randomly generated scenarios vs some kind of blend
	- Logic vs feeling
	- should players choose their actions before rolling dice, or should the dice dictate actions beforehand?
	- What element of randomness? Bones? Cards? Dice?
	- If dice, d6 or d% or what
	- Rule-of-cool vs Static
	- Dynamic modifiers vs Static Modifiers
	- Dynamic Terrain vs Static Terrain
	- Should players be able to create their own units
	- point-buy vs rolled vs randomized armies and units
	- should systems be individual or shared between phases
	- how many and what stats should individual units have
	- does it matter where a unit is facing
	- Anti-Vehicle spam vs Infantry Mobs
	- Should a small, mobile force have a chance against a large, armored mob?
	- Should points matter more than space?
	- How and Why are units extant?
	- Should armies be allowed to spawn off tabletop, and sweep on?
	- If a necromancer resurrects the dead, whose dead does he resurrect?
	- Do buffs run on a turn timer or another timer?
	- Is turn order decided by initiative, coin flip, or other function?
	- Is there a way to make a self-balancing game?
	- Cards vs Book-only vs downloadable stat blocks
3. Weight
	- Many rules vs Few rules
	- Intricacy vs Generalities
	- fine balance vs gross balance
4. Research
	- Whose ideas should we steal from?
	- Who has done these ideas before and well?
5. Scope
	- What level of detail and diversity should we have in rules?
	- Should the system be functional for a DnD type TTRPG session?
	- Where do core books end and splatbooks begin?
