# Open Source Wargame
*A world of vast implications*

**Open Source Wargame** *(Formerly jokingly referred to as BattleMace 40million)* is a wargaming system heavily inspired by the likes of Warhammer 40k (from 2nd edition -> 5th edition), Warhammer Fantasy, Warmachines/Hordes, Classic Napoleonics, Crimson Skies, Dust Warfare, and Battletech.

The aim is to create a system that evokes the feelings of these games, without being completely tied to the trappings of the timeframe or paradigm in which they were created.

Please stand by for further updates.

#### Ruleset

Currently under development.
see [Requirements](./Ruleset/requirements.md) & [Design](./Ruleset/Design.md) for more information.

#### Fluff

Fluff and alterations to rulesets to support it should be done independent of the creation of the base system, outside the creation of Debug factions/armies

#### Alterations

Raise issues using the 'issues' functionality of the repo

#### Ideas/Pull Requests/Support

see [Ideas](./Ideas.md)

#### Code of Conduct

If you feel you need one, see [Code of Conduct](./CoC.md)

#### Research

I am not the most versed in the creation of Wargames, nor am I the most experienced in playing them. If you would like to help research the project in your spare time, feel free. There is a list of items requiring some measure of reasearch which will assist in the creation of better systems.

See [Research](./Research/Research.md) & the [Research Template](./Research/Index.md#Template)

Please note that all content within the research section is copyright their respective owners. OSWG does not claim ownership of any subject researched.


