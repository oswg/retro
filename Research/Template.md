# Game, Template

#### Edition: **_**

#### Year: **_**

#### Summary

## Contents
  - [Mechanics Summary](#mechanics-summary)
    - [Game Procedures](#game-procedures)
    - [Turn Procedures](#turn-procedures)
    - [Army Procedures](#army-procedures)
    - [Win Conditions](#win-conditions)
    - [Other](#other)
    - [Unit Structure](#unit-structure)
  - [Simulated Game](#simulated-game)
  - [Points of Interest](#points-of-interest)
  - [Proposed Action](#proposed-action)
  - [Lessons to be Learned](#lessons)
  
---
## Mechanics Summary

### Game Procedures

### Turn Procedures

### Army Procedures

### Win/Loss Conditions

### Other

### Unit Structure

---
## Simulated Game
	
---
## Points of Interest


---
## Proposed Action

---

## Lessons to be Learned


---

  

##### This is a research document, and thus all items are copyright their respective owners.