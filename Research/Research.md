# Research

## Research In Pipeline:
	- [Warhammer 40k 8th Edition](40k8th.md)
	- Warhammer 40k 9th Edition
	- Warhammer 40k 7th Edition
	- Force on Force: Modern Wargaming Rules

## Main Research Requests:
	- Warmahordes
	- Napoleanic
	- Infinity
	- Warhammer Fantasy
	- Warhammer 40k
	- Age of Sigmar
	- Dust 1947+
	- Malifaux
	- Kings of War
	- Battletech
	- Necromunda
	- Gaslands

## Secondary Research Requests
	- FATE Core
	- Savage Worlds
	- d20 SRD
	- Pathfinder
	- Mekton
	- Cyberpunk 2020
	- d20 Modern
	- Warhammer Codices
	- D&D & other Splatbooks
	- Gurps
	- Shadowrun
	- Board Games w/ an element of stragegy


## Things to keep in mind
	1. Everything. Seriously, this is very early days.
		- Movement
			- Square/hex/inch/cm
		- Attack
		- Saves
		- Model Base shape and scale
		- Line of Sight
		- Vehicles
		- etc.
	
A more itemized list will be coming in the future


## Lessons
1. ***Book Layout*** The core rulebook is layed out in such a way to be logically understood by even the most novice player. This means Rules -> Lore -> Miniature Examples -> Hobbyist Corner -> Campaign Types -> Appendices -> Index [^7th]
2. ***Preparing for Battle / Fortunes of War*** Here, the core rulebook succinctly describes what is fun about 40k, a quick synopsis of a game, and what happens afterward - drinks down at the pub. There are photos of setup and action, the players, and objectives.[^7th]
3. ***Blast Templates*** Blast Templates are a good idea, though having them mathematically defined would be helpful to the end user. They do speed up the resolution to ranged and artillery combat. (3-D printable?)[^7th]
4. ***Clear Language*** At all relevant times, instead of 'roll a dice', the term D6 or six-sided die is used.[^7th]
5. Diagrams are your friends.[^8th]
6. Architecture of the main tome is important. Keep gameplay together.[^8th]
7. Glossary of game terms/concepts would be very helpful in cultivating a better class of player.[^8th]
8. Fluff should not be the primary selling point of your tabletop wargame.[^8th]
9. Do not be afraid to deemphasize a singular scenario of battle if it is not favored by the players.[^8th]
10. The clarity of language is paramount.[^8th]
11. An excess of words is not a viable substitute for design.[^8th]
12. A loss of confidence with the rule/release strategy should not be compensated with more setting exposition.[^8th]


[^7th]: [Warhammer 40,000, 7th Edition](./40k7th.md)
[^8th]: [Warhammer 40,000, 8th Edition](./40k8th.md)