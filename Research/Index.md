# Research

### Template

# Name: DefaultGame

## Edition: Xth

## Year: 200X

## Summary: 
	
	DefaultGame 

## Mechanics Summary: 
	
	Roll Dice, recieve Bacon

## Unit Structure:

 | name           | wp    | ap    | bacon |
 | :------------- | :---: | :---: | ----- |
 | default unit   | 3     | 4     |   yes |

## Simulated Game:

	blah blah blah roll dice whatever

## Points of Interest:

	1. Point[^1]

## Proposed Action:

	1. Steal main mechanics.
	2. ???
	3. Profit

---

[^1]:Book of Things, page 41, sections a through f
